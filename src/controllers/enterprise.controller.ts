import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,
  post,

  requestBody,
  response
} from '@loopback/rest';
import {Enterprise} from '../models';
import {EnterpriseRepository} from '../repositories';

export class EnterpriseController {
  constructor(
    @repository(EnterpriseRepository)
    public enterpriseRepository: EnterpriseRepository,
  ) { }

  @post('/enterprises')
  @response(200, {
    description: 'Enterprise model instance',
    content: {'application/json': {schema: getModelSchemaRef(Enterprise)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Enterprise, {
            title: 'NewEnterprise',
            exclude: ['id'],
          }),
        },
      },
    })
    enterprise: Omit<Enterprise, 'id'>,
  ): Promise<Enterprise> {
    return this.enterpriseRepository.create(enterprise);
  }

  @get('/enterprises/count')
  @response(200, {
    description: 'Enterprise model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Enterprise) where?: Where<Enterprise>,
  ): Promise<Count> {
    return this.enterpriseRepository.count(where);
  }

  @get('/enterprises')
  @response(200, {
    description: 'Array of Enterprise model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Enterprise, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Enterprise) filter?: Filter<Enterprise>,
  ): Promise<Enterprise[]> {
    return this.enterpriseRepository.find(filter);
  }

  @get('/enterprises/{id}')
  @response(200, {
    description: 'Enterprise model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Enterprise, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Enterprise, {exclude: 'where'}) filter?: FilterExcludingWhere<Enterprise>
  ): Promise<Enterprise> {
    return this.enterpriseRepository.findById(id, filter);
  }

  @del('/enterprises/{id}')
  @response(204, {
    description: 'Enterprise DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.enterpriseRepository.deleteById(id);
  }
}
