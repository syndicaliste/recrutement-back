import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
  response,
} from '@loopback/rest';
import {Disponibility} from '../models';
import {DisponibilityRepository} from '../repositories';

export class DisponibilityController {
  constructor(
    @repository(DisponibilityRepository)
    public disponibilityRepository : DisponibilityRepository,
  ) {}

  @post('/disponibilities')
  @response(200, {
    description: 'Disponibility model instance',
    content: {'application/json': {schema: getModelSchemaRef(Disponibility)}},
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Disponibility, {
            title: 'NewDisponibility',
            exclude: ['id'],
          }),
        },
      },
    })
    disponibility: Omit<Disponibility, 'id'>,
  ): Promise<Disponibility> {
    return this.disponibilityRepository.create(disponibility);
  }

  @get('/disponibilities/count')
  @response(200, {
    description: 'Disponibility model count',
    content: {'application/json': {schema: CountSchema}},
  })
  async count(
    @param.where(Disponibility) where?: Where<Disponibility>,
  ): Promise<Count> {
    return this.disponibilityRepository.count(where);
  }

  @get('/disponibilities')
  @response(200, {
    description: 'Array of Disponibility model instances',
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: getModelSchemaRef(Disponibility, {includeRelations: true}),
        },
      },
    },
  })
  async find(
    @param.filter(Disponibility) filter?: Filter<Disponibility>,
  ): Promise<Disponibility[]> {
    return this.disponibilityRepository.find(filter);
  }

  @patch('/disponibilities')
  @response(200, {
    description: 'Disponibility PATCH success count',
    content: {'application/json': {schema: CountSchema}},
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Disponibility, {partial: true}),
        },
      },
    })
    disponibility: Disponibility,
    @param.where(Disponibility) where?: Where<Disponibility>,
  ): Promise<Count> {
    return this.disponibilityRepository.updateAll(disponibility, where);
  }

  @get('/disponibilities/{id}')
  @response(200, {
    description: 'Disponibility model instance',
    content: {
      'application/json': {
        schema: getModelSchemaRef(Disponibility, {includeRelations: true}),
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Disponibility, {exclude: 'where'}) filter?: FilterExcludingWhere<Disponibility>
  ): Promise<Disponibility> {
    return this.disponibilityRepository.findById(id, filter);
  }

  @patch('/disponibilities/{id}')
  @response(204, {
    description: 'Disponibility PATCH success',
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Disponibility, {partial: true}),
        },
      },
    })
    disponibility: Disponibility,
  ): Promise<void> {
    await this.disponibilityRepository.updateById(id, disponibility);
  }

  @put('/disponibilities/{id}')
  @response(204, {
    description: 'Disponibility PUT success',
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() disponibility: Disponibility,
  ): Promise<void> {
    await this.disponibilityRepository.replaceById(id, disponibility);
  }

  @del('/disponibilities/{id}')
  @response(204, {
    description: 'Disponibility DELETE success',
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.disponibilityRepository.deleteById(id);
  }
}
