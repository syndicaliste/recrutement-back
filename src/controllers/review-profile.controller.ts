import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Review,
  Profile,
} from '../models';
import {ReviewRepository} from '../repositories';

export class ReviewProfileController {
  constructor(
    @repository(ReviewRepository)
    public reviewRepository: ReviewRepository,
  ) { }

  @get('/reviews/{id}/profile', {
    responses: {
      '200': {
        description: 'Profile belonging to Review',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Profile)},
          },
        },
      },
    },
  })
  async getProfile(
    @param.path.number('id') id: typeof Review.prototype.id,
  ): Promise<Profile> {
    return this.reviewRepository.profile(id);
  }
}
