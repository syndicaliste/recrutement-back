import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  Disponibility,
  Profile,
} from '../models';
import {DisponibilityRepository} from '../repositories';

export class DisponibilityProfileController {
  constructor(
    @repository(DisponibilityRepository)
    public disponibilityRepository: DisponibilityRepository,
  ) { }

  @get('/disponibilities/{id}/profile', {
    responses: {
      '200': {
        description: 'Profile belonging to Disponibility',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(Profile)},
          },
        },
      },
    },
  })
  async getProfile(
    @param.path.number('id') id: typeof Disponibility.prototype.id,
  ): Promise<Profile> {
    return this.disponibilityRepository.profile(id);
  }
}
