import {Entity, model, property} from '@loopback/repository';

@model()
export class Enterprise extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
  })
  adress: string;

  @property({
    type: 'number',
    required: true,
  })
  zipCode: number;

  @property({
    type: 'string',
    required: true,
  })
  city: string;

  @property({
    type: 'string',
    required: true,
  })
  country: string;

  @property({
    type: 'number',
    required: true,
  })
  phoneNumber: number;

  @property({
    type: 'string',
    required: true,
  })
  email: string;

  @property({
    type: 'string',
    required: true,
  })
  siege: string;

  @property({
    type: 'string',
    required: true,
  })
  siret: string;

  @property({
    type: 'string',
    required: true,
  })
  domaine: string;


  constructor(data?: Partial<Enterprise>) {
    super(data);
  }
}

export interface EnterpriseRelations {
  // describe navigational properties here
}

export type EnterpriseWithRelations = Enterprise & EnterpriseRelations;
