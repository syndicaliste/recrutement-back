import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Profile} from './profile.model';

@model()
export class Review extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
  })
  mark?: number;

  @property({
    type: 'string',
  })
  comment?: string;

  @belongsTo(() => Profile)
  profileId: number;

  constructor(data?: Partial<Review>) {
    super(data);
  }
}

export interface ReviewRelations {
  // describe navigational properties here
}

export type ReviewWithRelations = Review & ReviewRelations;
