import {Entity, model, property, belongsTo} from '@loopback/repository';
import {Profile} from './profile.model';

@model()
export class Disponibility extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'date',
    required: true,
  })
  start: string;

  @property({
    type: 'date',
    required: true,
  })
  end: string;

  @property({
    type: 'string',
  })
  commentaire?: string;

  @belongsTo(() => Profile)
  profileId: number;

  constructor(data?: Partial<Disponibility>) {
    super(data);
  }
}

export interface DisponibilityRelations {
  // describe navigational properties here
}

export type DisponibilityWithRelations = Disponibility & DisponibilityRelations;
