import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Review, ReviewRelations, Profile} from '../models';
import {ProfileRepository} from './profile.repository';

export class ReviewRepository extends DefaultCrudRepository<
  Review,
  typeof Review.prototype.id,
  ReviewRelations
> {

  public readonly profile: BelongsToAccessor<Profile, typeof Review.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('ProfileRepository') protected profileRepositoryGetter: Getter<ProfileRepository>,
  ) {
    super(Review, dataSource);
    this.profile = this.createBelongsToAccessorFor('profile', profileRepositoryGetter,);
    this.registerInclusionResolver('profile', this.profile.inclusionResolver);
  }
}
