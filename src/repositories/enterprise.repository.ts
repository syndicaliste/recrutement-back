import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Enterprise, EnterpriseRelations} from '../models';

export class EnterpriseRepository extends DefaultCrudRepository<
  Enterprise,
  typeof Enterprise.prototype.id,
  EnterpriseRelations
> {
  constructor(
    @inject('datasources.db') dataSource: DbDataSource,
  ) {
    super(Enterprise, dataSource);
  }
}
