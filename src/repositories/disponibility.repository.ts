import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor} from '@loopback/repository';
import {DbDataSource} from '../datasources';
import {Disponibility, DisponibilityRelations, Profile} from '../models';
import {ProfileRepository} from './profile.repository';

export class DisponibilityRepository extends DefaultCrudRepository<
  Disponibility,
  typeof Disponibility.prototype.id,
  DisponibilityRelations
> {

  public readonly profile: BelongsToAccessor<Profile, typeof Disponibility.prototype.id>;

  constructor(
    @inject('datasources.db') dataSource: DbDataSource, @repository.getter('ProfileRepository') protected profileRepositoryGetter: Getter<ProfileRepository>,
  ) {
    super(Disponibility, dataSource);
    this.profile = this.createBelongsToAccessorFor('profile', profileRepositoryGetter,);
    this.registerInclusionResolver('profile', this.profile.inclusionResolver);
  }
}
